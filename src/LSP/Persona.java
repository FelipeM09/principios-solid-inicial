/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LSP;

/**
 *
 * @author USUARIO
 */
public class Persona {

    private String cc;
    private String nombre;
    private String apellidos;
    private String tarjeta;

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public Persona(String dni, String nombre, String apellidos, String tarjeta) {
        super();
        this.cc = dni;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.tarjeta = tarjeta;
    }

    public void pagar() {

        System.out.println("mi dni es " + getCc() + "pago con la tarjeta" + tarjeta);
    }
}
