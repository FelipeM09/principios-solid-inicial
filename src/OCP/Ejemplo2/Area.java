/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OCP.Ejemplo2;

/**
 *
 * @author LEONEL
 */
public class Area {
    //calase figura
    public double calcularArea(Polygon p) {
double result = 0;
    if (p.type==1){
        result = calcularAreaSquare((Square) p);
    } else if (p.type==2){
        result = calcularAreaCircle((Circle) p);
}
return result;
 }

    public Area() {
    }
 
    public double calcularAreaCircle(Circle circle) {
return Math.PI * Math.pow(circle.getRadius(),2);
}
 
    public double calcularAreaSquare(Square square) {
    return Math.pow(square.getSide(),2);
}
    
   
 }
  
 

